var draw; 
var m, c; 
var text_border = false;
var mouse = 0;
var eraser = false;
var hasInput = false;
var c_array = new Array();
var step = -1;
var x1, y1;
var temp_image;

function init() {
    m = document.getElementById("m"); 
    c = m.getContext("2d"); 
    c.lineWidth = 1;
    c_push();
}

function md() {
    c.moveTo(event.offsetX, event.offsetY);
    x1 = event.offsetX; //  shape要用的第一個點的x
    y1 = event.offsetY; //  shape要用的第一個點的y
    if (mouse == 3 || mouse == 4 || mouse == 5 || mouse == 6) {
        temp_image = c.getImageData(0, 0, m.width, m.height);
    }
    // 畫畫開關
    if (mouse == 0 || mouse == 3 || mouse == 4 || mouse == 5 || mouse == 6) {
        draw = true;
    } 
    else {
        draw = false;
    }
    // 橡皮擦開關
    if (mouse == 2) {
        eraser = true;
    }
    else {
        eraser = false;
    }
    //  筆畫變色
    var pen_color = document.getElementById('pen_color').value;
    c.strokeStyle = pen_color;
    //  新增文字框
    if (text_border) {
        textarea_onclick();
    }
    // 字型
    var word_type = document.getElementById("word_type").value;
    var word_size = document.getElementById("word_size").value;
    c.font = word_size + 'px' + ' ' + word_type;
    c.fillStyle = pen_color;
    // 筆型
    c.lineCap = "round";
    c.beginPath();
}

function mv() {
    if(draw) {
        if (mouse == 0) {       //  normal
            c.lineTo(event.offsetX, event.offsetY);
            c.stroke();
        }
        else if (mouse == 3) {  //  line
            c.putImageData(temp_image, 0, 0);
            c.beginPath();
            c.moveTo(x1, y1);
            c.lineTo(event.offsetX, event.offsetY);
            c.stroke();
        }
        else if (mouse == 4) {  //  rectangle
            c.putImageData(temp_image, 0, 0);
            c.strokeRect(x1, y1, event.offsetX - x1, event.offsetY - y1);
        }
        else if (mouse == 5) {  //  arc
            c.putImageData(temp_image, 0, 0);
            c.beginPath();
            c.arc(x1, y1, Math.sqrt((event.offsetX - x1) * (event.offsetX - x1) + (event.offsetY - y1) * (event.offsetY - y1) ), 0, 2 * Math.PI);
            c.stroke();
        }
        else {                  //  triangle
            c.putImageData(temp_image, 0, 0);
            c.beginPath();
            c.moveTo(x1, y1);
            c.lineTo(event.offsetX, event.offsetY);
            c.lineTo(event.offsetX + (x1 - event.offsetX) * 2, event.offsetY);
            c.lineTo(x1 , y1);
            c.stroke();
        }
    }
    if (eraser) {
        c.globalCompositeOperation = "destination-out";
        c.lineTo(event.offsetX, event.offsetY);
        c.stroke();
        c.globalCompositeOperation = "source-over";
    }
}

function mup() {
    draw = false;
    eraser = false;
    c.closePath();
    c_push();
}

////////////////////////////////////////////////////

//  triangle

function triangle_onclick() {
    text_border = false;
    mouse = 6;
}

//  arc

function arc_onclick() {
    text_border = false;
    mouse = 5;
}

//  rectangle

function rectangle_onclick() {
    text_border = false;
    mouse = 4;
}

//  line_shape

function line_onclick() {
    text_border = false;
    mouse = 3;
}

//  stack

function c_push() {
    step++;
    if (step < c_array.length) {
        c_array.length = step;
    }
    c_array.push(c.getImageData(0, 0, m.width, m.height));
}

//  undo

function undo_onclick() {
    if (step > 0) {
        step--;
        c.putImageData(c_array[step], 0, 0);
        console.log("undo");
    }
}

//  redo

function redo_onclick() {
    if (step < c_array.length - 1) {
        step++;
        c.putImageData(c_array[step], 0, 0);
        console.log("redo");
    }
}

//  存檔

function download_onclick() {
    var file = document.createElement('a');
    file.download = "image";
    file.href = m.toDataURL("image/png");
    file.click();
}

//  上傳

function image_onclick(id) {
    var inputObj = document.createElement('input');
    inputObj.addEventListener('change',readFile,false);
    inputObj.type = 'file';
    inputObj.accept = 'image/*';
    inputObj.id = id;
    inputObj.click();
    c_push();
}

function readFile(){
    var file = this.files[0];//獲取input輸入的圖片
    if(!/^image\/\w+/.test(file.type)){
    alert("請確保檔案為影象型別");
    return false;
    }//判斷是否圖片，在移動端由於瀏覽器對呼叫file型別處理不同，雖然加了accept = 'image/*'，但是還要再次判斷
    var reader = new FileReader();
    reader.readAsDataURL(file);//轉化成base64資料型別
    reader.onload = function(e){
        drawToCanvas(this.result);
    }
}


function drawToCanvas(imgData){
    var img = new Image();
    img.src = imgData;
    img.onload = function(){//必須onload之後再畫
        c.drawImage(img, 0, 0, m.width, m.height);
    }
}


//  文字框

function textarea_onclick() {
    if (hasInput) return;
    addInput(event.offsetX, event.offsetY);
}

function addInput(x, y) {

    var txt = document.createElement("input");
    txt.type = "text";

    txt.style.position = 'fixed';
    txt.style.left = event.offsetX + 'px';
    txt.style.top = event.offsetY + 'px';

    txt.onkeydown = handleEnter;

    document.body.appendChild(txt);

    txt.focus();

    hasInput = true;
}


function handleEnter(e) {
    var keyCode = e.keyCode;
    if (keyCode === 13) {
        drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
        document.body.removeChild(this);
        hasInput = false;
    }
}

function drawText(txt, x, y) {
    c.textBaseline = 'top';
    c.textAlign = 'left';
    c.fillText(txt, x - 4, y - 4);
}

//  筆刷

function brush_onclick() {      //  筆畫粗細
    var pen_font = document.getElementById('Brush-Size').value;
    c.lineWidth = pen_font;
    console.log(c.lineWidth);
}

//  筆

function pen_onclick() {
    text_border = false;
    mouse = 0;
}

//  文字

function text_onclick() {
    text_border = true;
    mouse = 1;
}

//  換鼠標

function change_mouse() {
    if (mouse == 0) {
        document.getElementById("m").style.cursor = "default";
    }
    else if (mouse == 1) {
        document.getElementById("m").style.cursor = "help";
    }
    else {
        document.getElementById("m").style.cursor = "crosshair";
    }
}

//  橡皮擦

function eraser_onclick() {
    text_border = false;
    mouse = 2;
}

//  清畫布

function clear_onclick() {
    c.clearRect(0, 0, m.width, m.height);
}





























