# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%      | N         |


---

### How to use 

Color:點進去調色盤選顏色
Brush-Size:拉slider調大小
Font:選字型
Font-size:輸入字型大小
pen:點(畫畫)
text:點(文字框按enter才會印到畫布上)
eraser:點(橡皮擦)
image:點(上傳圖片)
download:點(下載)
redo:點
undo:點
line_shape:點(拉直線)
rectangle_shape:點(拉矩形)
arc_shape:點(拉圓形)
triangle_shape:點(拉三角形)


### Function description

md():滑鼠按下去，而這邊有紀錄按下那一瞬間的點跟圖，以實作出shape，還有控制畫圖開關、橡皮擦開關、筆畫變色、新增文字框、字型

mv():滑鼠移動，主要控制畫線，把圖畫出來

mup():滑鼠放開

c_push():把圖片紀錄在array裡，控制index來實坐undo、redo

image_onclick():建一個type:file的object，然後用click()去執行，監聽到readfile()坐轉換成URL的狀態，送給drawToCanvas去畫圖。

addInput(x, y):建一個type:text的object，接下來傳到handleEnter，處理當按Enter來取消文字框，然後用drawText(x, y)來畫到圖上

change_mouse()是改變圖標(鼠標的部分由於時間的關係我只有坐pen、text，其他的則都設為"crosshair")

其他on_click基本上可以當成flag



### Gitlab page link

"https://108062139.gitlab.io/AS_01_WebCanvas"

### Others (Optional)

    Nothing.

<style>
table th{
    width: 100%;
}
</style>